class AddLessons < ActiveRecord::Migration
  def change
    create_table(:lessons) do |t|
      t.date :lesson_date
      t.string :name
      t.text :description
      t.text :resources
    end
  end
end
