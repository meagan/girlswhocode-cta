class LessonPresenter
  def initialize(lesson)
    @lesson = lesson
  end

  def display_date
    @lesson.lesson_date.strftime("%b %d, %Y")
  end
end
