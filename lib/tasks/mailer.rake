namespace :mailer do
  desc "Sends notification email"
  task :send_notification => :environment do
    puts "Sending Email to All Users"
    User.all.each do |user|
      NotificationMailer.email_notification(user).deliver
      puts "Successfully sent email to #{user.name}"
    end
  end
end
