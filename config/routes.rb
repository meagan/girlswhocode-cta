GirlsWhoCode::Application.routes.draw do
  devise_for :users
  get 'dashboard', to: 'pages#dashboard'
  root to: "pages#login", :as => '/'
  get 'resources', to: 'pages#resources'
  get 'about', to: 'pages#about'
  get 'lessons', to: 'lessons#index'
  get "lessons/new" => 'lessons#new', as: 'new_lesson'
  post "lessons/new" => 'lessons#create'
  get "lessons/:id", to: 'lessons#show', as: 'lesson'
  post "lessons/:id" => 'lessons#update'
  get "lessons/:id/edit", to: 'lessons#edit', as: 'edit_lesson'
  delete "lessons/:id" => 'lessons#destroy', as: 'destroy_lesson'


  get 'questions', to: 'questions#index'
  get 'questions/new' => 'questions#new', as: 'new_question'
  post 'questions/new' => 'questions#create'
  get 'questions/:id', to: 'questions#show', as: 'question'
  post 'questions/:id', to: 'questions#update'
  get 'questions/:id/edit', to: 'questions#edit', as: 'edit_question'
  delete 'questions/:id' => 'questions#destroy', as: 'destroy_question'
end
