require './lib/presenters/lesson_presenter'
class LessonsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_authorized, :only => [:new, :create, :edit, :update, :destroy]
  def index
    @lessons = Lesson.order('lesson_date DESC')
  end

  def new
    @lesson = Lesson.new
  end

  def create
    @lesson = Lesson.new(lesson_params)
    if @lesson.save
      redirect_to lesson_path(@lesson), :notice => "Lesson Saved."
    else
      redirect_to new_lesson_path, :notice => "Lesson Not Saved."
    end
  end

  def show
    @lesson = Lesson.find(params[:id])
    @presenter = LessonPresenter.new(@lesson)
  end

  def edit
    @lesson = Lesson.find(params[:id])
  end

  def update
    @lesson = Lesson.find(params[:id])
    if @lesson.update_attributes(lesson_params)
      redirect_to lesson_path(@lesson), :notice => "Lesson Updated."
    else
      flash[:notice] = @lesson.errors.full_messages
      redirect_to edit_lesson_path(@lesson)
    end
  end

  def destroy
    Lesson.destroy(params[:id])
    redirect_to(lessons_path, notice: "Successfully deleted lesson.")
  end

  protected
  def lesson_params
    params.require(:lesson).permit(:name, :lesson_date, :description, :resources)
  end
end
