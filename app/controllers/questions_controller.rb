class QuestionsController < ApplicationController
  def index
    @questions = Question.all
  end

  def new
    @question = Question.new
  end

  def create
    @question = Question.new(question_params)
    if @question.save
      redirect_to question_path(@question), :notice => "Thanks for your question!"
    else
      redirect_to new_question_path, :notice => "Make sure all fields all filled out."
    end
  end

  def show
    @question = Question.find(params[:id])
  end

  def edit
    @question = Question.find(params[:id])
  end

  def update
    @question = Question.find(params[:id])
    if @question.update_attributes(question_params)
      direct_to question_path, :notice => "Question Updated."
    else
      flash[:notice] = @question.errors.full_messages
      redirect_to edit_question_path(@question)
    end
  end

  protected
  def question_params
    params.require(:question).permit(:title, :description, :user_id)
  end
end
