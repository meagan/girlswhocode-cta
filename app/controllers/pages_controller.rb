class PagesController < ApplicationController
  def login
    if !current_user.nil?
      redirect_to '/dashboard'
    else
      render :layout => 'home'
    end
  end

  def dashboard
    @user = current_user
    @current_lesson = Lesson.last
  end

  def resources
  end

  def about
  end
end
