class NotificationMailer < ActionMailer::Base
  default :from => "meaganewaller@gmail.com"

  def email_notification(recipient)
    @user = recipient
    mail(to: @user.email,
         subject: "Don't Forget About About Class on Friday!",
         content_type: "text/html"
        )
  end
end
