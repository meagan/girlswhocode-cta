class Lesson < ActiveRecord::Base
  validates :name, :presence => true
  validates :lesson_date, :presence => true
  validates :description, :presence => true
  validates :resources, :presence => true
end
